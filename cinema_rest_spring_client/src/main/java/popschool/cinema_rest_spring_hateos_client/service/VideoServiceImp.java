package popschool.cinema_rest_spring_hateos_client.service;


import org.assertj.core.groups.Tuple;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import popschool.cinema_rest_spring_hateos_client.exception.ExceptionEXISTE;
import popschool.cinema_rest_spring_hateos_client.exception.ExceptionNOTFOUND;
import popschool.cinema_rest_spring_hateos_client.model.*;

import javax.xml.ws.Response;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;

@Service
public class VideoServiceImp implements VideoService {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                 Methode Table Emprunt                                                      //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public Emprunt emprunter(String nom, String titre) {
        String URL_CREATE_EMPLOYEE = "http://localhost:8081/emprunts/Nom/" + nom + "/titre/" + titre;

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate3 = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Emprunt> requestBody = new HttpEntity<>(headers);

        // Send request with POST method.
        try{
            ResponseEntity<Emprunt> reponse = restTemplate3.postForEntity(URL_CREATE_EMPLOYEE, requestBody, Emprunt.class);
            System.out.println(reponse.getStatusCode());
            return reponse.getBody();
        }catch (RestClientException e){
            if (e.getClass()== HttpClientErrorException.Conflict.class){
                throw new ExceptionEXISTE();
            }else {
                throw new ExceptionNOTFOUND();
            }
        }
    }

    @Override
    public void retourEmprunt(String nom, String titre) {
        String URL_CREATE_EMPLOYEE = "http://localhost:8081/emprunts/retour/Nom/" + nom + "/titre/" + titre;

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate3 = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Emprunt> requestBody = new HttpEntity<>(headers);

        // Send request with POST method.
        try{
            restTemplate3.put(URL_CREATE_EMPLOYEE, requestBody, Emprunt.class);
        }catch (RestClientException e){
            if (e.getClass()== HttpClientErrorException.Conflict.class){
                throw new ExceptionEXISTE();
            }else {
                throw new ExceptionNOTFOUND();
            }
        }
    }

    @Override
    public List<Emprunt> findAllEmprunt() {
        final String URL_EMPRUNT ="http://localhost:8081/emprunts";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Emprunt[]> reponse = restTemplate.getForEntity(URL_EMPRUNT, Emprunt[].class);
            List<Emprunt> emprunts = Arrays.asList(reponse.getBody());
            return emprunts;
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public List<Emprunt> empruntNonRendu() {
        final String URL_EMPRUNT ="http://localhost:8081/emprunts/nonRendu";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Emprunt[]> reponse = restTemplate.getForEntity(URL_EMPRUNT, Emprunt[].class);
            List<Emprunt> emprunts = Arrays.asList(reponse.getBody());
            return emprunts;
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public Emprunt empruntById(Long id) {
        final String URL_EMPRUNT ="http://localhost:8081/emprunts/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Emprunt> reponse = restTemplate.getForEntity(URL_EMPRUNT, Emprunt.class);
            return reponse.getBody();
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                    Methode Table Client                                                    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public List<Client> ensClient() {
        final String URL_ALL_CLIENT ="http://localhost:8081/clients";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Client[]> reponse = restTemplate.getForEntity(URL_ALL_CLIENT, Client[].class);
            List<Client> clients = Arrays.asList(reponse.getBody());
            return clients;
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public Client getClientById(Long id) {
        final String URL_ALL_CLIENT ="http://localhost:8081/clients/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Client> reponse = restTemplate.getForEntity(URL_ALL_CLIENT, Client.class);

            return reponse.getBody();
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public Client createClient(Client client) {
        String URL_CREATE_CLIENT = "http://localhost:8081/clients/create";

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Client> requestBody = new HttpEntity<>(client , headers);

        // Send request with POST method.
        try{
            ResponseEntity<Client> reponse = restTemplate.postForEntity(URL_CREATE_CLIENT, requestBody, Client.class);
            System.out.println(reponse.getStatusCode());
            return reponse.getBody();
        }catch (RestClientException e){
            if (e.getClass()== HttpClientErrorException.Conflict.class){
                throw new ExceptionEXISTE();
            }else {
                throw new ExceptionNOTFOUND();
            }
        }
    }

    @Override
    public Client updateClient(Client client) {
        String URL_CREATE_CLIENT = "http://localhost:8081/clients/update";

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Client> requestBody = new HttpEntity<>(client , headers);

        // Send request with POST method.
        try{
            Client reponse = restTemplate.patchForObject(URL_CREATE_CLIENT, requestBody, Client.class);
            return reponse;
        }catch (RestClientException e){
            if (e.getClass()== HttpClientErrorException.Conflict.class){
                throw new ExceptionEXISTE();
            }else {
                throw new ExceptionNOTFOUND();
            }
        }
    }

    @Override
    public void deleteClient(Client client) {
        String URL_DELETE_CLIENT = "http://localhost:8081/clients/delete";

        System.out.println("///////////////////////////////////");
        System.out.println(client);
        System.out.println("///////////////////////////////////");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        headers.setContentType(MediaType.APPLICATION_JSON);

        RestTemplate restTemplate = new RestTemplate();

        // Data attached to the request.
        HttpEntity<Client> requestBody = new HttpEntity<>(client , headers);

        // Send request with POST method.
        try{
            restTemplate.delete(URL_DELETE_CLIENT, client);
        }catch (RestClientException e){
            if (e.getClass()== HttpClientErrorException.Conflict.class){
                throw new ExceptionEXISTE();
            }else {
                throw new ExceptionNOTFOUND();
            }
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                    Methode Table Film                                                      //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Film> ensFilmEmpruntable() {
        final String URL_FILM ="http://localhost:8081/films/emprumtable";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Film[]> reponse = restTemplate.getForEntity(URL_FILM, Film[].class);
            List<Film> films = Arrays.asList(reponse.getBody());
            return films;
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public List<Film> ensFilm(){
        final String URL_FILM ="http://localhost:8081/films";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Film[]> reponse = restTemplate.getForEntity(URL_FILM, Film[].class);
            List<Film> films = Arrays.asList(reponse.getBody());
            return films;
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public List<Film> ensFilmGenre(String genre) {
        final String URL_FILM ="http://localhost:8081/films/genre/" + genre;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Film[]> reponse = restTemplate.getForEntity(URL_FILM, Film[].class);
            List<Film> films = Arrays.asList(reponse.getBody());
            return films;
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public List<Tuple> infoRealisateurActeur(String titre) {
        final String URL_FILM ="http://localhost:8081/films/real/acteur/" + titre;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Tuple[]> reponse = restTemplate.getForEntity(URL_FILM, Tuple[].class);
            List<Tuple> tuples = Arrays.asList(reponse.getBody());
            return tuples;
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public int nbrFilmDuGenre(String genre) {
        final String URL_FILM ="http://localhost:8081/films/nbFilms/" + genre;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Integer> reponse = restTemplate.getForEntity(URL_FILM, Integer.class);
            return reponse.getBody();
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public List<Film> ensFilmEmpruntesByClient(Optional<Client> client) {
        final String URL_FILM ="http://localhost:8081/films/nbFilms/" + client;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Film[]> reponse = restTemplate.getForEntity(URL_FILM, Film[].class);
            List<Film> ensFilm = Arrays.asList(reponse.getBody());
            return ensFilm;
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public List<Film> findByGenre_Nature(String nature) {
        final String URL_GENRE ="http://localhost:8081/films/nature/" + nature;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Film[]> reponse = restTemplate.getForEntity(URL_GENRE, Film[].class);
            List<Film> films = Arrays.asList(reponse.getBody());
            return films;
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                    Methode Table Client                                                    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Genre> ensGenre() {
        final String URL_GENRE ="http://localhost:8081/genres";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Genre[]> reponse = restTemplate.getForEntity(URL_GENRE, Genre[].class);
            List<Genre> genres = Arrays.asList(reponse.getBody());
            return genres;
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                    Methode Table ACTEUR                                                    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Acteur> ensActeur() {
        final String URL_GENRE ="http://localhost:8081/acteurs";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Acteur[]> reponse = restTemplate.getForEntity(URL_GENRE, Acteur[].class);
            List<Acteur> acteurs = Arrays.asList(reponse.getBody());
            return acteurs;
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public Acteur getActeurByNom(String nom) {
        final String URL_GENRE ="http://localhost:8081/acteurs/"+ nom;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<Acteur> reponse = restTemplate.getForEntity(URL_GENRE, Acteur.class);
            return reponse.getBody();
        }catch (RestClientException e){
            throw new ExceptionNOTFOUND();
        }
    }

}
