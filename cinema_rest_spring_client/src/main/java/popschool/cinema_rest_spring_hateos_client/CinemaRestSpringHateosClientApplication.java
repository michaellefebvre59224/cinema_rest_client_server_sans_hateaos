package popschool.cinema_rest_spring_hateos_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemaRestSpringHateosClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(CinemaRestSpringHateosClientApplication.class, args);
    }

}
