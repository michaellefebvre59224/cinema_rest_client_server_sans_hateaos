package popschool.cinema_rest_spring_hateos_client.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Data
public class Acteur {

    private Long nacteur;
    private List<Film> films = new ArrayList<>();
    private String nom;
    private String prenom;
    private Date naissance;
    private Pays pays;
    private Integer nbrefilms;
    public Pays getPays() {
        return pays;
    }
    public void setPays(Pays pays) {
        this.pays = pays;
    }
    protected Acteur(){}

    @Override
    public String toString() {
        return "Acteur{" +
                "nacteur=" + nacteur +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", naissance=" + naissance +
                ", pays=" + pays +
                ", nbrefilms=" + nbrefilms +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Acteur)) return false;
        Acteur acteur = (Acteur) o;
        return nom.equals(acteur.nom) &&
                prenom.equals(acteur.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }
}
