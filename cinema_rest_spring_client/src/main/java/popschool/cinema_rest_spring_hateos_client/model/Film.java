package popschool.cinema_rest_spring_hateos_client.model;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class Film {

    private Long nfilm;
    private String titre;
    private Genre genre;
    private Date sortie;
    private Pays pays;
    private String realisateur;
    private Acteur acteur;
    private Long entrees;
    private String oscar;
    private List<Emprunt> emprunt;

    @Override
    public String toString() {
        return "Film{" +
                "nfilm=" + nfilm +
                ", titre='" + titre + '\'' +
                ", genre=" + genre +
                ", sortie=" + sortie +
                ", pays=" + pays +
                ", realisateur='" + realisateur + '\'' +
                ", acteur=" + acteur +
                ", entrees=" + entrees +
                ", oscar='" + oscar + '\'' +
                '}';
    }
}
