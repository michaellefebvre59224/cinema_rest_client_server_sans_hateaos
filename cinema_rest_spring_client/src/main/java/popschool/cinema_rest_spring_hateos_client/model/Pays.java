package popschool.cinema_rest_spring_hateos_client.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Pays {

    private Long npays;
    private String nom;
    private List<Acteur> acteurs = new ArrayList<>();
    private List<Film> films = new ArrayList<>();

    @Override
    public String toString() {
        return "Pays{" +
                ", nom='" + nom + '\'' +
                '}';
    }
}
