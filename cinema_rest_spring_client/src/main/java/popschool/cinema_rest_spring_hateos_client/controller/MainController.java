package popschool.cinema_rest_spring_hateos_client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import popschool.cinema_rest_spring_hateos_client.model.Client;
import popschool.cinema_rest_spring_hateos_client.model.Emprunt;
import popschool.cinema_rest_spring_hateos_client.service.VideoService;
;

@Controller
public class MainController {

    @Autowired
    private VideoService videoService;

    //----------------------------------------------------------------------------------------------------------------//
    //TODO                                      AFFICHER LES FILMS D'UN GENRE
    //----------------------------------------------------------------------------------------------------------------//
    //renvoie à la page selection genre et recupère la liste des genres
    @RequestMapping(value = {"/selectionGenre"}, method = RequestMethod.GET)
    public String selectionGenre(Model model) {

        model.addAttribute("listegenre", videoService.ensGenre());

        return "selectionGenre";
    }

    //renvoie à la page listfilm et recupère la liste des film du genre selectionné
    @PostMapping("/voirfilm" )
    public String films(@RequestParam("genrechoisi")String nature, Model model){
        model.addAttribute("ensfilmchoisi", videoService.findByGenre_Nature(nature));
        return "listfilms";
    }

    //----------------------------------------------------------------------------------------------------------------//
    //TODO                       AFFICHER LES ACTEUR ET LA LISTYE DE FILM D'UN ACTEUR CHOISI
    //----------------------------------------------------------------------------------------------------------------//
    @RequestMapping(value = {"/listeacteur"}, method = RequestMethod.GET)
    public String listeacteur(Model model) {

        model.addAttribute("ensacteur", videoService.ensActeur());

        return "listacteur";
    }

    //renvoie à la page listfilm et recupère la liste des film du genre selectionné
    @PostMapping("/voirfilmbyacteur" )
    public String voirfilmbyacteur(@RequestParam("acteurchoisi")String nomacteur, Model model){
        model.addAttribute("ensfilmchoisi", videoService.getActeurByNom(nomacteur));
        return "listfilms";
    }

    //----------------------------------------------------------------------------------------------------------------//
    //TODO                                          EMPRUNTER UN FILM
    //----------------------------------------------------------------------------------------------------------------//
    @RequestMapping(value = {"/emprunter"}, method = RequestMethod.GET)
    public String listclientandfilm(Model model) {

        model.addAttribute("ensclient", videoService.ensClient());
        model.addAttribute("ensfilm", videoService.ensFilmEmpruntable());

        return "emprunter";
    }
    @PostMapping(value = {"/empruntersave"})
    public String emprunter(@RequestParam("clientchoisi")String clientchoisi, @RequestParam("filmchoisi")String filmchoisi, Model model) {

        videoService.emprunter(clientchoisi, filmchoisi);
        model.addAttribute("ensemprunt", videoService.findAllEmprunt());
        return "listemmprunt";

    }

    @RequestMapping(value = {"/listemprunter"}, method = RequestMethod.GET)
    public String listemprunt(Model model) {

        model.addAttribute("ensemprunt", videoService.findAllEmprunt());

        return "listemmprunt";
    }

    //----------------------------------------------------------------------------------------------------------------//
    //TODO                                          LISTE DES EMRPRUNT
    //----------------------------------------------------------------------------------------------------------------//
    @RequestMapping(value = {"/listdesemprunt"}, method = RequestMethod.GET)
    public String listdesemprunt(Model model) {

        model.addAttribute("ensemprunt", videoService.empruntNonRendu());

        return "retouremprunt";
    }
    //----------------------------------------------------------------------------------------------------------------//
    //TODO                                          RENDRE UN FILM
    //----------------------------------------------------------------------------------------------------------------//
    @PostMapping(value = {"/retouremprunt"})
    public String retouremprunter(@RequestParam("empruntchoisi")Long empruntchoisi, Model model) {

        Emprunt emprunt = videoService.empruntById(empruntchoisi);
        videoService.retourEmprunt(emprunt.getClient().getNom(),emprunt.getFilm().getTitre());
        model.addAttribute("ensemprunt", videoService.findAllEmprunt());
        return "listemmprunt";

    }

    //----------------------------------------------------------------------------------------------------------------//
    //TODO                                          GESTION CLIENT
    //----------------------------------------------------------------------------------------------------------------//
    @RequestMapping(value = {"/gererclient"}, method = RequestMethod.GET)
    public String gestionclient(Model model) {

        model.addAttribute("ensclient", videoService.ensClient());
        Client newclient = new Client();
        model.addAttribute("newclient", newclient);

        return "gestionclient";
    }

    //Ajouter les clients
    @PostMapping(value = {"/saveclient"})
    public String nouveauclient(Model model, @ModelAttribute("newclient") Client newclient ){
        videoService.createClient(newclient);
        return "redirect:/gererclient";
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id")Long id, Model model){
        Client client = videoService.getClientById(id);
        videoService.deleteClient(client);
        return "redirect:/gererclient";
    }

    @GetMapping("/edit/{id}")
    public String updateUser(@PathVariable("id")Long id, Model model){
        Client client = videoService.getClientById(id);
        model.addAttribute("newclient", client);
        return "/updateclient";
    }

    @PostMapping("/modifclient/{id}")
    public String updateUser(@PathVariable("id") long id, @ModelAttribute("newclient") Client newclient, Model model) {
        newclient.setNclient(id);
        videoService.updateClient(newclient);
        return "redirect:/gererclient";
    }

}
