package popschool.cinema_rest_spring_hateaos_server.dao;


import org.springframework.data.repository.CrudRepository;
import popschool.cinema_rest_spring_hateaos_server.model.Client;


import java.util.List;
import java.util.Optional;

public interface ClientRepository extends CrudRepository<Client, Long> {

    //Trouver un client en fonction du nom
    Optional<Client> findByNomLike(String nom);

    //Trouver tous les client ordonnés par nom
    List<Client> findAllByOrderByNom();



}
