package popschool.cinema_rest_spring_hateaos_server.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.cinema_rest_spring_hateaos_server.model.Emprunt;


import java.util.List;
import java.util.Optional;

public interface EmpruntRepository extends CrudRepository<Emprunt, Long> {


    Optional<Emprunt> findByClient_NomAndFilm_TitreAndRetour(String nom, String titre, String non);
    List<Emprunt> findByRetour(String retour);
}
