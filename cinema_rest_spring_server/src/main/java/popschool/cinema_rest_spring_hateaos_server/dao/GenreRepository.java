package popschool.cinema_rest_spring_hateaos_server.dao;

import org.springframework.data.repository.CrudRepository;
import popschool.cinema_rest_spring_hateaos_server.model.Genre;


public interface GenreRepository extends CrudRepository<Genre, Long> {



}
