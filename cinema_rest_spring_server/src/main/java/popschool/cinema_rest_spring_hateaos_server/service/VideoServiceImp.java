package popschool.cinema_rest_spring_hateaos_server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import popschool.cinema_rest_spring_hateaos_server.dao.*;
import popschool.cinema_rest_spring_hateaos_server.exception.ExceptionEXISTE;
import popschool.cinema_rest_spring_hateaos_server.exception.ExceptionNOTFOUND;
import popschool.cinema_rest_spring_hateaos_server.model.*;


import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class VideoServiceImp implements VideoService {

    @Autowired
    private EmpruntRepository empruntRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private ActeurRepository acteurRepository;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                 Methode Table Emprunt                                                      //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public Emprunt emprunter(String nom, String titre) throws ExceptionNOTFOUND, ExceptionEXISTE {
        if (nom==null)throw new ExceptionNOTFOUND();
        if (titre==null)throw new ExceptionNOTFOUND();
        Optional<Client> client = clientRepository.findByNomLike(nom);
        Optional<Film> film = filmRepository.findByTitreLike(titre);
        try{
            return empruntRepository.save(new Emprunt(client.get(), film.get(), "NON", new Date()));
        }catch (Exception ex){
            System.out.println(ex);
            throw new ExceptionEXISTE();
        }
    }

    @Override
    public Emprunt retourEmprunt(String nom, String titre) {
        if (nom==null)throw new ExceptionNOTFOUND();
        if (titre==null)throw new ExceptionNOTFOUND();
        try {
            Optional<Emprunt> emprunt = empruntRepository.findByClient_NomAndFilm_TitreAndRetour(nom,titre,"NON");
            Emprunt emp = emprunt.get();
            emp.setRetour("OUI");
            return empruntRepository.save(emp);
        }catch (Exception ex){
            throw new ExceptionEXISTE();
        }

    }

    @Override
    public List<Emprunt> findAllEmprunt() {

        return (List<Emprunt>) empruntRepository.findAll();
    }

    @Override
    public List<Emprunt> empruntNonRendu() {
        return empruntRepository.findByRetour("NON");
    }

    @Override
    public Emprunt empruntById(Long id) throws ExceptionNOTFOUND{
        if (id==null) throw new ExceptionNOTFOUND();
        return empruntRepository.findById(id).get();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                    Methode Table Client                                                    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Client> ensClient() {
        return clientRepository.findAllByOrderByNom();
    }

    @Override
    public Client getClientById(Long id) throws ExceptionNOTFOUND{
        if (id==null) throw new ExceptionNOTFOUND();
        return clientRepository.findById(id).get();
    }

    @Override
    public Client createClient(Client client) {
        if (client==null)throw new ExceptionNOTFOUND();
        try {
            return clientRepository.save(client);
        }catch (Exception e){
            throw new ExceptionEXISTE();
        }
    }

    @Override
    public Void deleteClient(Client client) {

        System.out.println("///////////// dans la methode delete //////////////////");
        System.out.println(client);
        System.out.println("///////////// dans la methode delete //////////////////");

        if (client==null)throw new ExceptionNOTFOUND();
        try {
            clientRepository.delete(client);
            return null;
        }catch (Exception e){
            throw new ExceptionNOTFOUND();
        }
    }

    @Override
    public Client updateClient(Client client) {
        if (client==null)throw new ExceptionNOTFOUND();
        try {
            clientRepository.findById(client.getNclient());
        }catch (Exception ex){
            throw new ExceptionNOTFOUND();
        }
        try{
            return clientRepository.save(client);
        }catch (Exception e){
            throw new ExceptionEXISTE();
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                    Methode Table Film                                                      //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Film> ensFilmEmpruntable() {
        return filmRepository.findAllFilmEmpruntable();
    }

    @Override
    public List<Film> ensFilm(){
        return filmRepository.findAllByOrderByTitre();
    }

    @Override
    public List<Film> ensFilmGenre(String genre) throws ExceptionNOTFOUND{
        if (genre==null) throw new ExceptionNOTFOUND();
        return filmRepository.findByGenre_Nature(genre);
    }

    @Override
    public List<Tuple> infoRealisateurActeur(String titre) throws ExceptionNOTFOUND{
        if (titre==null) throw new ExceptionNOTFOUND();
        return filmRepository.findinfoRealisateurAndActeur(titre);
    }

    @Override
    public int nbrFilmDuGenre(String genre) throws ExceptionNOTFOUND{
        if (genre==null) throw new ExceptionNOTFOUND();
        return filmRepository.findNbreFilmsByOneGenre(genre);
    }

    @Override
    public List<Film> ensFilmEmpruntesByClient(Client client) throws ExceptionNOTFOUND{
        if (client==null) throw new ExceptionNOTFOUND();
        return filmRepository.findFilmEmprunteByClient(client);
    }

    @Override
    public List<Film> findByGenre_Nature(String nature) {
        return filmRepository.findByGenre_Nature(nature);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                         GENRE                                                              //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Genre> ensGenre() {
        return (List<Genre>) genreRepository.findAll();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                         ACTEUR                                                             //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Acteur> ensActeur() {
        return (List<Acteur>) acteurRepository.findAll();
    }

    @Override
    public Acteur findActeurByNom(String nom) {
        return acteurRepository.findByNomLike(nom).get();
    }
}
