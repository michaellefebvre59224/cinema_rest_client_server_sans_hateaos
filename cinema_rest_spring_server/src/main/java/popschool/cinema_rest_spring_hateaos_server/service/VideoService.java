package popschool.cinema_rest_spring_hateaos_server.service;



import popschool.cinema_rest_spring_hateaos_server.exception.ExceptionEXISTE;
import popschool.cinema_rest_spring_hateaos_server.exception.ExceptionNOTFOUND;
import popschool.cinema_rest_spring_hateaos_server.model.*;

import javax.persistence.Tuple;
import java.util.List;
import java.util.Optional;


public interface VideoService{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                          EMPRUNT                                                           //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Emprunt emprunter(String nom, String titre) throws ExceptionNOTFOUND, ExceptionEXISTE;
    Emprunt retourEmprunt(String nom, String titre);
    List<Emprunt> findAllEmprunt();
    List<Emprunt> empruntNonRendu();
    Emprunt empruntById(Long id) throws ExceptionNOTFOUND;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                           CLIENT                                                           //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    List<Client> ensClient();
    Client getClientById(Long id) throws ExceptionNOTFOUND;
    Client createClient(Client client);
    Void deleteClient(Client client);
    Client updateClient(Client client);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                         FILM                                                               //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    List<Film> ensFilmEmpruntable();
    List<Film> ensFilm();
    List<Film> ensFilmGenre(String genre) throws ExceptionNOTFOUND;
    List<Tuple>infoRealisateurActeur(String titre) throws ExceptionNOTFOUND;
    int nbrFilmDuGenre(String genre) throws ExceptionNOTFOUND;
    List<Film> ensFilmEmpruntesByClient(Client client) throws ExceptionNOTFOUND;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                         FILM                                                               //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    List<Genre> ensGenre();
    List<Film> findByGenre_Nature(String nature);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //TODO                                         ACTEUR                                                             //
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    List<Acteur> ensActeur();
    Acteur findActeurByNom(String nom);
}
