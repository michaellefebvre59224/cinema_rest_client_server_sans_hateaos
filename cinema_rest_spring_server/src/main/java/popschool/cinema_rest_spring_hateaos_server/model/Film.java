package popschool.cinema_rest_spring_hateaos_server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name="film")
public class Film {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long nfilm;

    private String titre;

    @ManyToOne
    @JoinColumn(name = "ngenre")
    private Genre genre;

    @Temporal(TemporalType.DATE)
    private Date sortie;

    @ManyToOne
    @JoinColumn(name = "npays")
    private Pays pays;

    private String realisateur;

    @ManyToOne
    @JoinColumn(name ="nacteurprincipal")
    private Acteur acteur;

    private Long entrees;

    private String oscar;

    @OneToMany(mappedBy = "film")
    @JsonIgnore
    private List<Emprunt> emprunt;

    @Override
    public String toString() {
        return "Film{" +
                "nfilm=" + nfilm +
                ", titre='" + titre + '\'' +
                ", genre=" + genre +
                ", sortie=" + sortie +
                ", pays=" + pays +
                ", realisateur='" + realisateur + '\'' +
                ", acteur=" + acteur +
                ", entrees=" + entrees +
                ", oscar='" + oscar + '\'' +
                '}';
    }
}
