package popschool.cinema_rest_spring_hateaos_server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name="pays")
public class Pays {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long npays;

    private String nom;

    @OneToMany(mappedBy = "pays")
    @JsonIgnore
    private List<Acteur> acteurs = new ArrayList<>();

    @OneToMany(mappedBy = "pays")
    @JsonIgnore
    private List<Film> films = new ArrayList<>();

    @Override
    public String toString() {
        return "Pays{" +
                ", nom='" + nom + '\'' +
                '}';
    }
}
