package popschool.cinema_rest_spring_hateaos_server.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.cinema_rest_spring_hateaos_server.exception.ExceptionEXISTE;
import popschool.cinema_rest_spring_hateaos_server.exception.ExceptionNOTFOUND;
import popschool.cinema_rest_spring_hateaos_server.model.Client;
import popschool.cinema_rest_spring_hateaos_server.model.Emprunt;
import popschool.cinema_rest_spring_hateaos_server.model.Film;
import popschool.cinema_rest_spring_hateaos_server.service.VideoService;

import javax.persistence.Tuple;
import java.util.List;

@RestController
public class ControlerEmprunt {

    @Autowired
    private VideoService videoService;

    @GetMapping (value = "/emprunts")
    ResponseEntity<List<Emprunt>> getAllEmprunt(){
        try {
            List<Emprunt> ensEmprunt = videoService.findAllEmprunt();
            return ResponseEntity.status(HttpStatus.OK).body(ensEmprunt);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping (value = "/emprunts/nonRendu")
    ResponseEntity<List<Emprunt>> getAllEmpruntNonRendu(){
        try {
            List<Emprunt> ensEmprunt = videoService.empruntNonRendu();
            return ResponseEntity.status(HttpStatus.OK).body(ensEmprunt);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }


    @GetMapping (value = "/emprunts/{id}")
    ResponseEntity<Emprunt> getEmpruntById(@PathVariable("id") Long id){
        if (id==null) throw new ExceptionNOTFOUND();
        try {
            Emprunt emprunt = videoService.empruntById(id);
            return ResponseEntity.status(HttpStatus.OK).body(emprunt);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }catch (ExceptionEXISTE ex){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
    }

    @PostMapping(value = "/emprunts/Nom/{nom}/titre/{titre}")
    ResponseEntity<Emprunt> postEmprunt(@PathVariable("nom") String nom, @PathVariable("titre") String titre){
        try {
            Emprunt emprunt = videoService.emprunter(nom,titre);
            return ResponseEntity.status(HttpStatus.OK).body(emprunt);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }catch (ExceptionEXISTE ex){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
    }

    @PutMapping(value = "/emprunts/retour/Nom/{nom}/titre/{titre}")
    ResponseEntity<Emprunt> putRetourEmprunt(@PathVariable("nom") String nom, @PathVariable("titre") String titre){
        try {
            Emprunt emprunt = videoService.retourEmprunt(nom,titre);
            return ResponseEntity.status(HttpStatus.OK).body(emprunt);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
