package popschool.cinema_rest_spring_hateaos_server.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import popschool.cinema_rest_spring_hateaos_server.exception.ExceptionNOTFOUND;
import popschool.cinema_rest_spring_hateaos_server.model.Client;
import popschool.cinema_rest_spring_hateaos_server.model.Genre;
import popschool.cinema_rest_spring_hateaos_server.service.VideoService;

import java.util.List;

@RestController
public class ControlerGenre {

    @Autowired
    private VideoService videoService;

    @GetMapping (value = "/genres")
    ResponseEntity<List<Genre>> getAllGenre(){
        try {
            List<Genre> ensGenre = videoService.ensGenre();
            return ResponseEntity.status(HttpStatus.OK).body(ensGenre);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping (value = "/genres/{nature}")
    ResponseEntity<List<Genre>> getAllGenre(@PathVariable("nature") String nature){
        try {
            List<Genre> ensGenre = videoService.ensGenre();
            return ResponseEntity.status(HttpStatus.OK).body(ensGenre);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }
}
