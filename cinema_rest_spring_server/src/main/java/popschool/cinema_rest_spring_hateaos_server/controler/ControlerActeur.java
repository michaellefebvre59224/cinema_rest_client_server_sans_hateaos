package popschool.cinema_rest_spring_hateaos_server.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import popschool.cinema_rest_spring_hateaos_server.exception.ExceptionNOTFOUND;
import popschool.cinema_rest_spring_hateaos_server.model.Acteur;
import popschool.cinema_rest_spring_hateaos_server.model.Client;
import popschool.cinema_rest_spring_hateaos_server.service.VideoService;

import java.util.List;

@RestController
public class ControlerActeur {

    @Autowired
    private VideoService videoService;

    @GetMapping(value = "/acteurs")
    ResponseEntity<List<Acteur>> getAllActeur(){
        try {
            List<Acteur> ensClient = videoService.ensActeur();
            return ResponseEntity.status(HttpStatus.OK).body(ensClient);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping(value = "/acteurs/{nom}")
    ResponseEntity<Acteur> getAllActeur(@PathVariable("nom") String nom){
        if (nom==null) throw new ExceptionNOTFOUND();
        try {
            Acteur acteur = videoService.findActeurByNom(nom);
            return ResponseEntity.status(HttpStatus.OK).body(acteur);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

}
