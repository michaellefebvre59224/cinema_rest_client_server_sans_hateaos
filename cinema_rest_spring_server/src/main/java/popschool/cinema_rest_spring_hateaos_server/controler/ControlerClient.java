package popschool.cinema_rest_spring_hateaos_server.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import popschool.cinema_rest_spring_hateaos_server.exception.ExceptionEXISTE;
import popschool.cinema_rest_spring_hateaos_server.exception.ExceptionNOTFOUND;
import popschool.cinema_rest_spring_hateaos_server.model.Client;
import popschool.cinema_rest_spring_hateaos_server.service.VideoService;

import java.util.List;

@RestController
public class ControlerClient {

    @Autowired
    private VideoService videoService;

    @GetMapping (value = "/clients")
    ResponseEntity<List<Client>> getAllClient(){
        try {
            List<Client> ensClient = videoService.ensClient();
            return ResponseEntity.status(HttpStatus.OK).body(ensClient);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping (value = "/clients/{id}")
    ResponseEntity<Client> getClientId(@PathVariable("id") Long id){
        try {
            Client client = videoService.getClientById(id);
            return ResponseEntity.status(HttpStatus.OK).body(client);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @PostMapping (value = "/clients/create")
    ResponseEntity<Client> createClient(@RequestBody Client client){
        try {
            Client clientRep = videoService.createClient(client);
            return ResponseEntity.status(HttpStatus.CREATED).body(clientRep);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }catch (ExceptionEXISTE ex){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
    }

    @DeleteMapping (value = "/clients/delete")
    ResponseEntity<Void> deleteClient(@RequestBody Client client){
        System.out.println("////////////////////////////////////////////////////////");
        System.out.println(client);
        System.out.println("////////////////////////////////////////////////////////");
        try {
            videoService.deleteClient(client);
            return ResponseEntity.status(HttpStatus.OK).body(null);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }catch (ExceptionEXISTE ex){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
    }

    @PatchMapping (value = "/clients/update")
    ResponseEntity<Client> updateClient(@RequestBody Client client){
        try {
            Client clientRep = videoService.updateClient(client);
            return ResponseEntity.status(HttpStatus.OK).body(clientRep);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }catch (ExceptionEXISTE ex){
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
    }

}
