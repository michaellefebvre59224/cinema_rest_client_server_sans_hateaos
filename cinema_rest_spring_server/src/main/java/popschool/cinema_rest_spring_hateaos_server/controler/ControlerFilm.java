package popschool.cinema_rest_spring_hateaos_server.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import popschool.cinema_rest_spring_hateaos_server.exception.ExceptionNOTFOUND;
import popschool.cinema_rest_spring_hateaos_server.model.Client;
import popschool.cinema_rest_spring_hateaos_server.model.Film;
import popschool.cinema_rest_spring_hateaos_server.service.VideoService;

import javax.persistence.Tuple;
import java.util.List;
import java.util.Optional;

@RestController
public class ControlerFilm {

    @Autowired
    private VideoService videoService;

    @GetMapping (value = "/films/emprumtable")
    ResponseEntity<List<Film>> getAllFilmEmprumtable(){
        try {
            List<Film> ensFilm = videoService.ensFilmEmpruntable();
            return ResponseEntity.status(HttpStatus.OK).body(ensFilm);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping (value = "/films")
    ResponseEntity<List<Film>> getAllFilm(){
        try {
            List<Film> ensFilm = videoService.ensFilm();
            return ResponseEntity.status(HttpStatus.OK).body(ensFilm);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping (value = "/films/genre/{genre}")
    ResponseEntity<List<Film>> getAllFilmByGenre(@PathVariable("genre") String genre){
        if (genre==null)throw new ExceptionNOTFOUND();
        try {
            List<Film> ensFilm = videoService.ensFilmGenre(genre);
            return ResponseEntity.status(HttpStatus.OK).body(ensFilm);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping (value = "/films/real/acteur/{titre}")
    ResponseEntity<List<Tuple>> getInfoRealAndActeur(@PathVariable("titre") String titre){
        if (titre==null)throw new ExceptionNOTFOUND();
        try {
            List<Tuple> info = videoService.infoRealisateurActeur(titre);
            return ResponseEntity.status(HttpStatus.OK).body(info);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping (value = "/films/nbFilms/{genre}")
    ResponseEntity<Integer> getNbFilmByGenre(@PathVariable("genre") String genre){
        if (genre==null)throw new ExceptionNOTFOUND();
        try {
            Integer info = videoService.nbrFilmDuGenre(genre);
            return ResponseEntity.status(HttpStatus.OK).body(info);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping (value = "/films/emprunte/{client}")
    ResponseEntity<List<Film>> getFilmEmprunte(@PathVariable("client") Client client){
        if (client==null)throw new ExceptionNOTFOUND();
        try {
            videoService.getClientById(client.getNclient());
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        try {
            List<Film> ensFilm = videoService.ensFilmEmpruntesByClient(client);
            return ResponseEntity.status(HttpStatus.OK).body(ensFilm);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    @GetMapping (value = "/films/nature/{nature}")
    ResponseEntity<List<Film>> getFilmEmprunte(@PathVariable("nature")String nature){
        if (nature==null)throw new ExceptionNOTFOUND();
        try {
            List<Film> ensFilm = videoService.findByGenre_Nature(nature);
            return ResponseEntity.status(HttpStatus.OK).body(ensFilm);
        }catch (ExceptionNOTFOUND e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }


}
